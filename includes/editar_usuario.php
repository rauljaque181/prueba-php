<?php

    include("db.php");

    $correo = $_GET['email'];

    $query  = "SELECT * FROM user where email = '$correo' ";
    $result = mysqli_query($conn, $query);
    $row    = mysqli_fetch_array($result);

?>

<div class="container p-4">
    <div >
        <div>
            <div class="card card-body">
                <form action="update_user.php" method="POST">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" name="nombre" class="form-control" placeholder="Juan" id="nombre" value="<?php echo $row['name']?>" autofocus>
                    </div>
                    <div class="form-group">
                        <label for="apellido">Apellido</label>
                        <input type="text" name="apellido" class="form-control" placeholder="Perez" id="apellido"  value="<?php echo $row['lastname']?>" autofocus>
                    </div>
                    <div class="form-group">
                        <label for="correo">Correo</label>
                        <input type="email" name="correo" class="form-control" placeholder="email@email.com" value="<?php echo $row['email']?>" readonly autofocus>
                    </div>
                    <div class="form-group">
                        <label for="password">Contraseña</label>
                        <input type="text" name="password" class="form-control" placeholder="***********" id="password" value="<?php echo $row['password']?>" autofocus>
                    </div>
                    <input type="submit" class="btn btn-success btn-block" name="save_user" value="Actualizar">
                </form>
            </div>
        </div>
    </div>
</div>