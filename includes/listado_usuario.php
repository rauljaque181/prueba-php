<div class="col-md-8 container p-4">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Correo</th>
                <th>Fecha de creación</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
            <?php 

                $query = "SELECT * FROM user ORDER BY created_at DESC";
                $result = mysqli_query($conn, $query);

                while($row = mysqli_fetch_array($result)){
            ?>
                <tr>
                    <td>
                        <?php 
                            echo $row['name']
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $row['lastname']
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $row['email']
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $row['created_at']
                        ?>
                    </td>
                    <td>
                        <center>
                            <a class="btn btn-info" href="editar_usuario.php?email=<?php echo $row['email']?>">Editar</a>
                        </center>
                    </td>
                    <td>
                        <center>
                            <a class="btn btn-danger" href="delete_user.php?email=<?php echo $row['email']?>">Eliminar</a>
                        </center>
                    </td>
                </tr>
            <?php }
            ?>
        </tbody>
    </table>
</div>