<div class="col-md-8 container p-4">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Nmbre</th>
                <th>Apellido</th>
                <th>Total Orden</th>
                <th>Fecha de creación Orden</th>
            </tr>
        </thead>
        <tbody>
            <?php 

                $query = "SELECT user.name, user.lastname, orders.order_total, orders.created_at 
                          From orders 
                          Join user on user.id = orders.user_id 
                          WHERE Year(user.created_at) >= 2003 AND Year(user.created_at) <= 2015 AND orders.created_at > '2003-01-01'
                          ORDER BY orders.created_at ASC";
                $result = mysqli_query($conn, $query);

                while($row = mysqli_fetch_array($result)){
            ?>
                <tr>
                    <td>
                        <?php 
                            echo $row['name']
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $row['lastname']
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $row['order_total']
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $row['created_at']
                        ?>
                    </td>
                </tr>
            <?php }
            ?>
        </tbody>
    </table>
</div>