<div class="container p-4">
    <div >
        <div>
            <div class="card card-body">
                <form action="save_user.php" method="POST">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" name="nombre" class="form-control" placeholder="Juan" id="nombre" autofocus>
                    </div>
                    <div class="form-group">
                        <label for="apellido">Apellido</label>
                        <input type="text" name="apellido" class="form-control" placeholder="Perez" id="apellido" autofocus>
                    </div>
                    <div class="form-group">
                        <label for="correo">Correo</label>
                        <input type="email" name="correo" class="form-control" placeholder="email@email.com" autofocus>
                    </div>
                    <div class="form-group">
                        <label for="password">Contraseña</label>
                        <input type="text" name="password" class="form-control" placeholder="***********" id="password" autofocus>
                    </div>
                    <input type="submit" class="btn btn-success btn-block" name="save_user" value="Guardar">
                </form>
            </div>
        </div>
    </div>
</div>